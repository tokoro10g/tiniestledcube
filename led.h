#pragma once
#include "stm32f30x.h"
#include "stm32f30x_conf.h"

struct LEDPin {
	uint8_t a;
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

const struct LEDPin pinmap[6];

volatile uint16_t ledDuty[6][3];

void resetLEDPins();
void selectLED(uint8_t id);
void setHiZLED(GPIO_TypeDef* GPIOx, uint16_t pin);
