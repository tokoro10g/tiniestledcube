/**
  ******************************************************************************
  * @file    stm32f30x_it.c 
  * @author  MCD Application Team
  * @version V1.2.2
  * @date    14-August-2015
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x_it.h"
#include "stm32f30x.h"
#include "stm32f30x_conf.h"
#include "config.h"
#include "led.h"

/** @addtogroup STM32F30x_StdPeriph_Templates
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
}

void TIM2_IRQHandler(void){
	static uint8_t targetLED = 1;
	if(TIM_GetITStatus(TIM2,TIM_IT_CC1)!=RESET){
		TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);
		setHiZLED(pincombi[pinmap[targetLED-1].r-1].gpio, pincombi[pinmap[targetLED-1].r-1].pin);
	}
	if(TIM_GetITStatus(TIM2,TIM_IT_CC2)!=RESET){
		TIM_ClearITPendingBit(TIM2, TIM_IT_CC2);
		setHiZLED(pincombi[pinmap[targetLED-1].g-1].gpio, pincombi[pinmap[targetLED-1].g-1].pin);
	}
	if(TIM_GetITStatus(TIM2,TIM_IT_CC3)!=RESET){
		TIM_ClearITPendingBit(TIM2, TIM_IT_CC3);
		setHiZLED(pincombi[pinmap[targetLED-1].b-1].gpio, pincombi[pinmap[targetLED-1].b-1].pin);
	}
	if(TIM_GetITStatus(TIM2,TIM_IT_Update)!=RESET){
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		TIM2->CCR1 = ledDuty[targetLED-1][0];
		TIM2->CCR2 = ledDuty[targetLED-1][1];
		TIM2->CCR3 = ledDuty[targetLED-1][2];
		//uint8_t state = GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_3);
		//GPIO_WriteBit(GPIOB, GPIO_Pin_3, !state);
		targetLED++;
		if(targetLED > 6) targetLED = 1;
		resetLEDPins();
		selectLED(targetLED);
	}
}

void TIM3_IRQHandler(void){
	static int8_t sign1[]={1,1,-1,-1,1,1},
		   sign2[]={1,-1,1,-1,1,-1},
		   sign3[]={-1,-1,1,1,-1,-1};
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)!=RESET){
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		for(uint8_t i=0; i<6; i++) {
			if(ledDuty[i][0] > 4400) sign1[i] = -1;
			else if(ledDuty[i][0] < 300) sign1[i] = 1;
			ledDuty[i][0] = (int16_t)ledDuty[i][0] + (int16_t)sign1[i]*10;

			if(ledDuty[i][1] > 5900) sign2[i] = -1;
			else if(ledDuty[i][1] < 400) sign2[i] = 1;
			ledDuty[i][1] = (int16_t)ledDuty[i][1] + (int16_t)sign2[i]*15;

			if(ledDuty[i][2] > 5900) sign3[i] = -1;
			else if(ledDuty[i][2] < 400) sign3[i] = 1;
			ledDuty[i][2] = (int16_t)ledDuty[i][2] + (int16_t)sign3[i]*12;
		}
	}
}
/******************************************************************************/
/*                 STM32F30x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f30x.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
