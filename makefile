SHELL = /bin/sh
TARGET_ARCH   = -mcpu=cortex-m3 -mthumb
INCLUDE_DIRS  = -I ./ \
				-I ../../Libraries/STM32F30x/Libraries/STM32F30x_StdPeriph_Driver/inc \
				-I ../../Libraries/STM32F30x/Libraries/CMSIS/Device/ST/STM32F30x/Include \
				-I ../../Libraries/STM32F30x/Libraries/CMSIS/Include
STARTUP_DIR = ../../Libraries/STM32F30x/Libraries/CMSIS/Device/ST/STM32F30x/Source/Templates/gcc_ride7/
BOARD_OPTS = -DHSE_VALUE=\(\(uint32_t\)8000000\) -DSTM32F30X
FIRMWARE_OPTS = -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS  = -MMD -Os -g3 -fsigned-char -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(BOARD_OPTS) $(FIRMWARE_OPTS)

CC      = arm-none-eabi-gcc
AS      = $(CC)
LD      = $(CC)
AR      = arm-none-eabi-ar
OBJCOPY = arm-none-eabi-objcopy
CFLAGS  = -std=gnu99 $(COMPILE_OPTS)
ASFLAGS = -x assembler-with-cpp -c $(TARGET_ARCH) $(COMPILE_OPTS) 
LDFLAGS = -Wl,--gc-sections,-Map=bin/main.map,-cref -T STM32F303K8.ld $(INCLUDE_DIRS)

DEPS = main.d

all: bin/main.hex

# many of xxx.o are compiled by suffix rule automatically
LIB_OBJS = $(sort \
 $(patsubst %.c,%.o,$(wildcard ../../Libraries/STM32F30x/Libraries/STM32F30x_StdPeriph_Driver/src/*.c)))

$(LIB_OBJS): \
 $(wildcard ../../Libraries/STM32F30x/Libraries/STM32F10x_StdPeriph_Driver/inc/*.h) \
 $(wildcard ../../Libraries/STM32F30x/Libraries/STM32F10x_StdPeriph_Driver/src/*.c) \
 $(wildcard ../../Libraries/STM32F30x/Libraries/CMSIS/Device/ST/STM32F30x/Include/*.h)

# main.o is compiled by suffix rule automatucally
bin/main.hex: $(LIB_OBJS) $(patsubst %.c,%.o,$(wildcard *.c)) $(patsubst %.cpp,%.o,$(wildcard *.cpp)) $(STARTUP_DIR)startup_stm32f30x.o
	$(LD) $(LDFLAGS) $(TARGET_ARCH) $^ -o bin/main.elf 
	$(OBJCOPY) -O ihex bin/main.elf bin/main.hex
 
clean:
	rm -rf *.o *.s bin/*
