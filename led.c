#include "stm32f30x.h"
#include "stm32f30x_conf.h"
#include "config.h"
#include "led.h"

const struct LEDPin pinmap[6] = {
	{8,1,4,5},
	{1,8,7,2},
	{3,4,1,2},
	{6,7,8,5},
	{4,3,6,5},
	{7,6,3,2}
};

volatile uint16_t ledDuty[6][3] = {
	{4000, 4000, 4000},
	{1000, 2000, 2000},
	{4000, 2000, 2000},
	{1000, 5000, 2000},
	{2000, 4000, 1000},
	{4000, 5000, 5000}
};

void resetLEDPins() {
	for(uint8_t i=0; i<8; i++){
		pincombi[i].gpio->MODER &= ~(GPIO_MODER_MODER0 << (pincombi[i].pin * 2));
		pincombi[i].gpio->MODER |= (((uint32_t)GPIO_Mode_IN) << (pincombi[i].pin * 2));
	}
}

void selectLED(uint8_t id) {
	id--;
	/*
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

	GPIO_InitStructure.GPIO_Pin = pincombi[pinmap[id].r-1].pin;
	GPIO_Init(pincombi[pinmap[id].r-1].gpio, &GPIO_InitStructure);
	GPIO_WriteBit(pincombi[pinmap[id].r-1].gpio, pincombi[pinmap[id].r-1].pin, Bit_RESET);

	GPIO_InitStructure.GPIO_Pin = pincombi[pinmap[id].g-1].pin;
	GPIO_Init(pincombi[pinmap[id].g-1].gpio, &GPIO_InitStructure);
	GPIO_WriteBit(pincombi[pinmap[id].g-1].gpio, pincombi[pinmap[id].g-1].pin, Bit_RESET);

	GPIO_InitStructure.GPIO_Pin = pincombi[pinmap[id].b-1].pin;
	GPIO_Init(pincombi[pinmap[id].b-1].gpio, &GPIO_InitStructure);
	GPIO_WriteBit(pincombi[pinmap[id].b-1].gpio, pincombi[pinmap[id].b-1].pin, Bit_RESET);

	GPIO_InitStructure.GPIO_Pin = pincombi[pinmap[id].a-1].pin;
	GPIO_Init(pincombi[pinmap[id].a-1].gpio, &GPIO_InitStructure);
	GPIO_WriteBit(pincombi[pinmap[id].a-1].gpio, pincombi[pinmap[id].a-1].pin, Bit_SET);
	*/

	pincombi[pinmap[id].r-1].gpio->OTYPER &= ~((GPIO_OTYPER_OT_0) << ((uint16_t)pincombi[pinmap[id].r-1].pin));
	pincombi[pinmap[id].r-1].gpio->OTYPER |= (uint16_t)(((uint16_t)GPIO_OType_PP) << ((uint16_t)pincombi[pinmap[id].r-1].pin));
	pincombi[pinmap[id].g-1].gpio->OTYPER &= ~((GPIO_OTYPER_OT_0) << ((uint16_t)pincombi[pinmap[id].g-1].pin));
	pincombi[pinmap[id].g-1].gpio->OTYPER |= (uint16_t)(((uint16_t)GPIO_OType_PP) << ((uint16_t)pincombi[pinmap[id].g-1].pin));
	pincombi[pinmap[id].b-1].gpio->OTYPER &= ~((GPIO_OTYPER_OT_0) << ((uint16_t)pincombi[pinmap[id].b-1].pin));
	pincombi[pinmap[id].b-1].gpio->OTYPER |= (uint16_t)(((uint16_t)GPIO_OType_PP) << ((uint16_t)pincombi[pinmap[id].b-1].pin));
	pincombi[pinmap[id].a-1].gpio->OTYPER &= ~((GPIO_OTYPER_OT_0) << ((uint16_t)pincombi[pinmap[id].a-1].pin));
	pincombi[pinmap[id].a-1].gpio->OTYPER |= (uint16_t)(((uint16_t)GPIO_OType_PP) << ((uint16_t)pincombi[pinmap[id].a-1].pin));

	pincombi[pinmap[id].r-1].gpio->MODER &= ~(GPIO_MODER_MODER0 << (pincombi[pinmap[id].r-1].pin * 2));
	pincombi[pinmap[id].r-1].gpio->MODER |= (((uint32_t)GPIO_Mode_OUT) << (pincombi[pinmap[id].r-1].pin * 2));
	pincombi[pinmap[id].g-1].gpio->MODER &= ~(GPIO_MODER_MODER0 << (pincombi[pinmap[id].g-1].pin * 2));
	pincombi[pinmap[id].g-1].gpio->MODER |= (((uint32_t)GPIO_Mode_OUT) << (pincombi[pinmap[id].g-1].pin * 2));
	pincombi[pinmap[id].b-1].gpio->MODER &= ~(GPIO_MODER_MODER0 << (pincombi[pinmap[id].b-1].pin * 2));
	pincombi[pinmap[id].b-1].gpio->MODER |= (((uint32_t)GPIO_Mode_OUT) << (pincombi[pinmap[id].b-1].pin * 2));
	pincombi[pinmap[id].a-1].gpio->MODER &= ~(GPIO_MODER_MODER0 << (pincombi[pinmap[id].a-1].pin * 2));
	pincombi[pinmap[id].a-1].gpio->MODER |= (((uint32_t)GPIO_Mode_OUT) << (pincombi[pinmap[id].a-1].pin * 2));

	pincombi[pinmap[id].r-1].gpio->BRR = (1<<pincombi[pinmap[id].r-1].pin);
	pincombi[pinmap[id].g-1].gpio->BRR = (1<<pincombi[pinmap[id].g-1].pin);
	pincombi[pinmap[id].b-1].gpio->BRR = (1<<pincombi[pinmap[id].b-1].pin);
	pincombi[pinmap[id].a-1].gpio->BSRR = (1<<pincombi[pinmap[id].a-1].pin);
}

void setHiZLED(GPIO_TypeDef* GPIOx, uint16_t pin) {
	GPIOx->MODER &= ~(GPIO_MODER_MODER0 << (pin * 2));
	GPIOx->MODER |= (((uint32_t)GPIO_Mode_IN) << (pin * 2));
}
