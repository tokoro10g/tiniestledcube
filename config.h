#pragma once

#include "stm32f30x.h"
#include "stm32f30x_conf.h"

struct PinCombination {
	GPIO_TypeDef* gpio;
	uint16_t pin;
};

const struct PinCombination pincombi[8];

void configClock();
void configTimer();
void configGpio();
